package com.level3_1.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ProductTypeDAO implements DAO {

    private static final String PRODUCT_TYPE_TABLE_DATA = "product_type.csv";
    private static final String INDEX_QUERY = "CREATE INDEX idx_product_type ON productType (product_type)";

    @Override
    public void insertData(Connection connection) {
        List<String> queries;
        queriesProcessing.createIndex(connection, INDEX_QUERY);
        try {
            queries = fileProcessing.getQueriesFromTheFile(PRODUCT_TYPE_TABLE_DATA);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            Statement statement = connection.createStatement();

            for (String query : queries) {
                statement.addBatch("INSERT INTO productType (product_type) VALUES" + " ('" + query + "')");
            }

            statement.executeBatch();
            connection.commit();
            logger.debug("Data into ProductType table inserted");

        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            logger.error("Problems with statement");
            ex.printStackTrace();
        }
    }
}
