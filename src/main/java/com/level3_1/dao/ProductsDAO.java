package com.level3_1.dao;

import com.level3_1.DataProcessing;
import com.level3_1.dto.DTOGenerator;
import com.level3_1.dto.ProductDto;
import org.apache.commons.lang3.time.StopWatch;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ProductsDAO implements DAO {

    private static final int BATCH_SIZE = 1000;
    private static final int BATCH_SIZE_FOR_LOG = 10000;
    private static final String SQL_PRODUCTS_INSERT = "INSERT INTO products (product, type_id) VALUES (?, ?)";
    DTOGenerator dtoGenerator = new DTOGenerator();
    private static final String inputtedType = new DataProcessing().readOutputFormat();
    private static final String INDEX_QUERY = "CREATE INDEX idx_type_id ON products (type_id)";
//    private static final String INDEX_QUERY2 = "CREATE INDEX idx_type_id ON products (type_id)";

    @Override
    public void insertData(Connection connection) {

        List<ProductDto> productDtoList = dtoGenerator.generateDTOlist();
//        queriesProcessing.createIndex(connection, INDEX_QUERY);
        StopWatch watch = new StopWatch();
        watch.start();
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_PRODUCTS_INSERT);
            int count = 0;
            for (ProductDto dto : productDtoList) {
                statement.setString(1, dto.getProductName());
                statement.setInt(2, dto.getType_id());

                statement.addBatch();
                count++;

                if (count % BATCH_SIZE == 0) {
                    statement.executeBatch();
                    statement.clearBatch();
                }
                logBatchNum(count);
            }
            statement.executeBatch();
            logger.debug("Data into Products table inserted");
            watch.stop();
            connection.commit();
            double sec = watch.getTime() / 1000.0;
            logRPS(sec, count);

        } catch (SQLException e) {
            e.printStackTrace();

            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void logRPS(double sec, int count) {
        logger.info("Inserted RPS into products table is {} ", sec);
        logger.info("Inserted RPS rows in second {} ", count / sec);
        logger.info("Inserted rows {} ", count);
    }

    private void logBatchNum(int count) {
        if (count % BATCH_SIZE_FOR_LOG == 0) {
            logger.debug("{} batches into products table inserted", count);
        }
    }

//    public void createIndexProd(Connection connection) {
//        queriesProcessing.createIndex(connection, INDEX_QUERY);
//        queriesProcessing.createIndex(connection, INDEX_QUERY2);
//    }

//    public void getShopAddressWithTheLargestNumberOfGivenTypeProducts(Connection connection) {
//        StopWatch watch = new StopWatch();
//        watch.start();
//        try {
//            String sql = "SELECT shop.address, type.product_type, COUNT(*) AS amount FROM products " +
//                    "JOIN productType AS type ON products.type_id = type.type_id " +
//                    "JOIN shop ON products.shop_id = shop.shop_id " +
//                    "WHERE type.product_type = ? " +
//                    "GROUP BY shop.shop_id, shop.address, type.product_type " +
//                    "ORDER BY COUNT(*) DESC " +
//                    "LIMIT 1;";
//
//            PreparedStatement statement = connection.prepareStatement(sql);
//            statement.setString(1, inputtedType);
//
//            statement.executeBatch();
//
//            connection.commit();
//            watch.stop();
//            double sec = watch.getTime();
//            logger.info("Row found for {} seconds", sec);
//            printResult(statement);
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//
//            try {
//                connection.rollback();
//            } catch (SQLException ex) {
//                ex.printStackTrace();
//            }
//        }
//    }

//    private void printResult(PreparedStatement statement) throws SQLException {
//        ResultSet resultSet = statement.executeQuery();
//        while (resultSet.next()) {
//            logger.info("The largest number of products ({}) of the {} type is in the store at the address {}",
//                    resultSet.getString("amount"),
//                    resultSet.getString("product_type"),
//                    resultSet.getString("address"));
//        }
//    }
}

