package com.level3_1.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ShopDAO implements DAO {
    private static final String INDEX_QUERY = "CREATE INDEX idx_address ON shop (address)";
    private static final String SHOP_TABLE_DATA = "shop_address.csv";

    @Override
    public void insertData(Connection connection) {
        List<String> queries;
        queriesProcessing.createIndex(connection, INDEX_QUERY);
        try {
            queries = fileProcessing.getQueriesFromTheFile(SHOP_TABLE_DATA);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try {
            Statement statement = connection.createStatement();

            for (String query : queries) {
                statement.addBatch("INSERT INTO shop (address) VALUES" + " ('" + query + "')");
            }

            statement.executeBatch();
            connection.commit();
            logger.debug("Data into Shop table inserted");

        } catch (SQLException ex) {
            try {
                connection.rollback();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            logger.error("Problems with statement");
            ex.printStackTrace();
        }
    }
}
