package com.level3_1.dao;

import com.level3_1.FileProcessing;
import com.level3_1.QueriesProcessing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;

public interface DAO {

    Logger logger = LoggerFactory.getLogger(DAO.class);
    FileProcessing fileProcessing = new FileProcessing();
    QueriesProcessing queriesProcessing = new QueriesProcessing();

    void insertData(Connection connection);

}
