package com.level3_1.dto;

import jakarta.validation.constraints.Size;

public class ProductDto {

    @Size(max = 10, message = "Name length must not be longer then 10 symbols")
//    @Pattern(regexp = "^A?.*a.*", message = "Must be at least one letter 'а'")
    private String productName;
    private int type_id;

    public ProductDto(String productName, int type_id) {
        this.productName = productName;
        this.type_id = type_id;
    }

    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    @Override
    public String toString() {
        return "ProductDto{" +
                "productName='" + productName + '\'' +
                ", type_id=" + type_id +
                '}';
    }
}
