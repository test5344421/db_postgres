package com.level3_1.dto;

import com.level3_1.*;
import jakarta.validation.ConstraintViolation;
import org.apache.commons.lang3.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DTOGenerator {
    Properties properties = new FileProcessing().loadProperties();
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private final int quantityDTO = Integer.parseInt(properties.getProperty("quantityProducts"));
    int prodNameMaxLength = Integer.parseInt(properties.getProperty("prodNameMaxLength"));
    int prodNameMinLength = Integer.parseInt(properties.getProperty("prodNameMinLength"));
    MyValidator validator = new MyValidator();
    QueriesProcessing queriesProcessing = new QueriesProcessing();
    Connection connection = ConnectionCreator.createConnection();
    Random random = new Random();
    public List<ProductDto> generateDTOlist() {
        logger.debug("Generating products list");
        List<Integer> shopId = queriesProcessing.getId(connection, "SELECT id FROM shop");
        List<Integer> typeId = queriesProcessing.getId(connection, "SELECT id FROM productType");
        StopWatch watch = new StopWatch();
        watch.start();

        List<ProductDto> listDTO = Stream.generate(() -> new ProductDto
                        (generateProductName(), generateRandomTypeId(typeId)))
                .filter(this::isValid)
                .limit(quantityDTO)
                .collect(Collectors.toCollection(LinkedList::new));
        watch.stop();
        double seconds = watch.getTime() / 1000.0;
        logRPS(listDTO, seconds);

        return listDTO;
    }

    private void logRPS(List<ProductDto> listDTO, double seconds) {
        logger.info("Generated {} DTO", listDTO.size());
        logger.info("RPS of generating is {} ", seconds);
        logger.info("RPS DTO in second {} ", listDTO.size() / seconds);
    }

    protected String generateProductName() {
        return textGenerator();
    }

    public int generateRandomShopId(List<Integer> shopId) {
        int index = random.nextInt(shopId.size());
        return shopId.get(index);
    }

    public int generateRandomProductId(List<Integer> productId) {
        int index = random.nextInt(productId.size());
        return productId.get(index);
    }

    public int generateRandomTypeId(List<Integer> typeId) {
        int index = random.nextInt(typeId.size());
        return typeId.get(index);
    }

    protected String textGenerator() {

        int length = ThreadLocalRandom.current().nextInt(prodNameMinLength, prodNameMaxLength);
        int FIRST_LETTER = 97;
        int LAST_LETTER = 123;

        return ThreadLocalRandom.current()
                .ints(length - 1, FIRST_LETTER, LAST_LETTER)
                .mapToObj(codePoint -> String.valueOf((char) codePoint))
                .collect(Collectors.joining());
    }

    protected boolean isValid(ProductDto productDto) {

        Set<ConstraintViolation<ProductDto>> validateDTO = validator.validateDTO(productDto);
        return validateDTO.isEmpty();
    }
}


