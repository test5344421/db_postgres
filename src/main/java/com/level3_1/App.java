package com.level3_1;

import com.level3_1.dao.GeneralTableDAO;
import com.level3_1.dao.ProductTypeDAO;
import com.level3_1.dao.ProductsDAO;
import com.level3_1.dao.ShopDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.*;

public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) throws IOException, SQLException {
        logger.debug("Start program");

        QueriesProcessing queriesProcessing = new QueriesProcessing();
        GeneralTableDAO generalTableDAO = new GeneralTableDAO();
        ProductTypeDAO productTypeDAO = new ProductTypeDAO();
        ShopDAO shopDAO = new ShopDAO();
        ProductsDAO productsDAO = new ProductsDAO();

        Connection connection = ConnectionCreator.createConnection();
        queriesProcessing.createTables(connection);
        shopDAO.insertData(connection);
        productTypeDAO.insertData(connection);
        productsDAO.insertData(connection);
        generalTableDAO.insertData(connection);

        generalTableDAO.createIndexGen(connection);
        generalTableDAO.getShopAddressWithTheLargestNumberOfGivenTypeProducts(connection);

        ConnectionCreator.closeConnection(connection);
        logger.info("Program finished");
    }
}
